%% clear
clear all global
restoredefaultpath

%% add the path to th_ptb
addpath('/home/th/git/th_ptb/') % change this to where th_ptb is on your system

%% initialize the PTB
th_ptb.init_ptb('/home/th/git_other/Psychtoolbox-3/'); % change this to where PTB is on your system

%% get a configuration object
ptb_cfg = th_ptb.PTB_Config();

%% do the configuration
ptb_cfg.fullscreen = false;
ptb_cfg.window_scale = 0.2;
ptb_cfg.skip_sync_test = true;
ptb_cfg.hide_mouse = false;

%% get th_ptb.PTB object
ptb = th_ptb.PTB.get_instance(ptb_cfg);

%% init audio and triggers
ptb.setup_screen;
ptb.setup_audio;
ptb.setup_trigger;

%% create or new Rectangle
my_rect = Rectangle(200, 150, [0 0 0]);

ptb.draw(my_rect);
ptb.flip();

%% we can scale and move it
my_rect.move(100, 0);
my_rect.scale(2);

ptb.draw(my_rect);
ptb.flip();

%% and we can do it in a loop...
my_rect = Rectangle(200, 150, [0 0 0]);

for i = 1:200
  my_rect.move(1, 1);
  
  ptb.draw(my_rect);
  ptb.flip();
end %for